//
//  GameScene.swift
//  RobotJump
//
//  Created by MacStudent on 2019-10-07.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    // MARK: SPRITE OUTLETS
    // ----------------------
    var robot:SKSpriteNode!
    
    override func didMove(to view: SKView) {
        print("Scene loaded!")
        
        // get robot from the SKS scene
        self.robot = self.childNode(withName: "robot") as! SKSpriteNode
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // when user taps teh screen, make robot jump
        //
        
        print("HELLO")
        
        let jumpAction = SKAction.applyImpulse(
            CGVector(dx:0, dy:3000),
            duration: 0.5)
        
        self.robot!.run(jumpAction)
        
    }
    
    
}
